#!/usr/bin/python

"""
    Doc goes here.
    I guess... ;)
"""

__version__ = '1.0.0'

import os
import sys

import SimpleHTTPServer
import BaseHTTPServer
import ConfigParser
import argparse

import pycurl
from StringIO import StringIO
import json

class ServerParameters:
    pass

def setup_environment():
    parser = argparse.ArgumentParser(description='SimpleHTTPMDServer to serve formatted Github flavored Markdown files.')
    parser.add_argument('-b,', '--bind', help='ip address to bind to, i.e. 192.168.0.25', default='127.0.0.1')
    parser.add_argument('-t,', '--port', help='port to use, i.e. 8090', type=int, default=8000)
    parser.add_argument('-u,', '--user', help='github user for API authentication', default='')
    parser.add_argument('-p,', '--password', help="github user's password for API authentication", default='')
    parser.add_argument('-c,', '--config', help="use config file specified", default='server.conf')

    args = parser.parse_args()

    Config = ConfigParser.ConfigParser()
    Config.read(args.config)

    server_options = {}
    github_options = {}

    if 'server' in Config.sections():
        server_options = dict(Config.items('server'))
    if 'github' in Config.sections():
        github_options = dict(Config.items('github'))

    print server_options
    print github_options

    # server config
    ServerParameters.HOST = server_options.get('host', args.bind)
    ServerParameters.PORT = int(server_options.get('port', args.port))
    ServerParameters.USER_AGENT = server_options.get('user-agent', 'SimpleHTTPServer 1.0')


    # github config
    ServerParameters.API_URL = github_options.get('github-api-url', 'https://api.github.com/markdown')

    ServerParameters.GITHUB_PERSONAL_ACCESS_TOKEN = github_options.get('github-token', '')

    GITHUB_USER = github_options.get('github-user', args.user)
    GITHUB_PASSWORD = github_options.get('github-password', args.password)

    ServerParameters.GITHUB_USER_PASSWORD = GITHUB_USER + ':' + GITHUB_PASSWORD if GITHUB_USER and GITHUB_PASSWORD else ''
    ServerParameters.GITHUB_USER = GITHUB_USER
    ServerParameters.GITHUB_PASSWORD = GITHUB_PASSWORD

    ServerParameters.CONTENT_TYPE = 'text/x-markdown'


    __location__ = os.path.realpath(os.path.dirname(__file__))
    ServerParameters.MARKDOWN_BODY_CSS = open(os.path.join(__location__, 'files/markdown_body.css'), 'r').read()
    ServerParameters.GITHUB_MARKDOWN_CSS = open(os.path.join(__location__, 'files/github_markdown.css'), 'r').read()
    ServerParameters.HTML_HEAD = open(os.path.join(__location__, 'files/head.html'), 'r').read()
    ServerParameters.HTML_TAIL = open(os.path.join(__location__, 'files/tail.html'), 'r').read()



def remove_prefix(text, prefix):
    if text.startswith(prefix):
        return text[len(prefix):]
    return text

def getGFM(filePath):
    buffer = StringIO()
    header = StringIO()

    with open(filePath, 'r') as content_file:
        markdownContent = content_file.read()

    headers = [
    #    'Authorization: token ' + GITHUB_PERSONAL_ACCESS_TOKEN,
        'Content-Type: ' + ServerParameters.CONTENT_TYPE,
        'User-Agent: ' + ServerParameters.USER_AGENT
    ]

    data = {
#        'mode': 'markdown',
        'mode': 'gfm',
        'text': markdownContent
    }
    postData = json.dumps(data)

    c = pycurl.Curl()

    c.setopt(c.URL, ServerParameters.API_URL)
    c.setopt(c.POST, True)
    if ServerParameters.GITHUB_USER_PASSWORD:
        c.setopt(c.USERPWD, ServerParameters.GITHUB_USER_PASSWORD)
    c.setopt(c.HTTPHEADER, headers)
    #c.setopt(c.POSTFIELDS, markdownContent)
    c.setopt(c.POSTFIELDS, postData)
    #c.setopt(c.WRITEDATA, buffer)
    #or
    c.setopt(c.WRITEFUNCTION, buffer.write)
    #if you need headers
    c.setopt(c.HEADERFUNCTION, header.write)

    # c.setopt(pycurl.SSL_VERIFYPEER, 1)
    # c.setopt(pycurl.SSL_VERIFYHOST, 2)
    # c.setopt(pycurl.CAINFO, "/path/to/updated-certificate-chain.pem")

    # c.setopt(pycurl.SSLCERTTYPE, "PEM")
    # c.setopt(pycurl.SSLCERT, "/path/to/client-cert.pem")
    # if the client cert file has password...
    # c.setopt(pycurl.KEYPASSWD, "<password here>")

    # or just use:
    c.setopt(pycurl.SSL_VERIFYPEER, 0)
    c.setopt(pycurl.SSL_VERIFYHOST, 0)

    #for debugging
    #c.setopt(c.VERBOSE, True)
    c.perform()
    c.close()

    body = buffer.getvalue()

    return body

class MyRequestHandler(SimpleHTTPServer.SimpleHTTPRequestHandler):
    def do_GET(self):
        path = remove_prefix(self.path, '/')

        if self.path.endswith(".md"):
            convertedMarkdown = getGFM(path)

            self.send_response(200)
            self.send_header('Content-Type', 'text/html; charset=utf-8')
            self.end_headers()

            self.wfile.write(ServerParameters.HTML_HEAD)
            self.wfile.write('<style>')
            self.wfile.write(ServerParameters.GITHUB_MARKDOWN_CSS)
            self.wfile.write('</style>')
            self.wfile.write('<style>')
            self.wfile.write(ServerParameters.MARKDOWN_BODY_CSS)
            self.wfile.write('</style>')
            self.wfile.write('<article class="markdown-body">')
            self.wfile.write(convertedMarkdown,)
            self.wfile.write('</article>')
            self.wfile.write(ServerParameters.HTML_TAIL)
            self.wfile.close()
        else:
            SimpleHTTPServer.SimpleHTTPRequestHandler.do_GET(self)

# if the script is called with user:pass parameter, get it
# if not, we'll use anonymous access which is limited to 60 calls per hour

if __name__ == '__main__':
    setup_environment()
    httpd = BaseHTTPServer.HTTPServer((ServerParameters.HOST, ServerParameters.PORT), MyRequestHandler)

    print "Serving at http://%s:%s/" % (ServerParameters.HOST, ServerParameters.PORT)
    httpd.serve_forever()

